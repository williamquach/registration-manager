package fr.william.quach;

import fr.william.quach.domain.models.payment.CreditCard.CreditCard;
import fr.william.quach.domain.models.payment.PaymentMethod;
import fr.william.quach.domain.models.user.Tradesman;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.domain.repositories.UserRepository;
import fr.william.quach.infrastructure.errors.users.InvalidUserGenericException;
import fr.william.quach.infrastructure.repositories.user.InMemoryUserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;

@SpringBootTest
public class UserTest {
    UserRepository userRepository = InMemoryUserRepository.getInstance();

    @Test
    public void should_create_a_user_correctly() throws InvalidUserGenericException {
        String userFirstname = "William";
        String userLastname = "QUACH";
        String userEmail = "wquach@myges.fr";
        String userPassword = "motDePasse";
        String creditCardNumbers = "0000000000000000";

        int expirationMonth = 9;
        int expirationYear = 2022;
        String creditCardCryptogram = "000";

        PaymentMethod creditCard = CreditCard.create(creditCardNumbers, expirationMonth, expirationYear, creditCardCryptogram);
        User user = User.create(userRepository.nextId(), userFirstname, userLastname, userEmail, userPassword, creditCard, new Tradesman());

        Assertions.assertEquals(userLastname, user.getLastname());
        Assertions.assertEquals(userEmail, user.getEmailAddress());
        Assertions.assertTrue(user.getPassword().isSame(userPassword));


        Assertions.assertEquals(expirationMonth - 1, ((CreditCard) user.getPaymentMethod()).getExpirationDate().getDate().get(Calendar.MONTH));
        Assertions.assertEquals(expirationYear, ((CreditCard) user.getPaymentMethod()).getExpirationDate().getDate().get(Calendar.YEAR));
        Assertions.assertEquals(creditCardCryptogram, ((CreditCard) user.getPaymentMethod()).getCryptogram());
    }

    @Test
    public void should_fail_when_creating_a_user_incorrectly() throws InvalidUserGenericException {
        String userFirstname = "William";
        String userLastname = "QUACH";
        String userEmail = "wquachmygesfr";
        String userPassword = "";
        String creditCardNumbers = "0000000000000000";

        int expirationMonth = 9;
        int expirationYear = 2022;
        String creditCardCryptogram = "000";

        CreditCard creditCard = CreditCard.create(creditCardNumbers, expirationMonth, expirationYear, creditCardCryptogram);
        Assertions.assertThrows(InvalidUserGenericException.class, () -> {
            User.create(userRepository.nextId(), userFirstname, userLastname, userEmail, userPassword, creditCard, new Tradesman());
        });
    }
}
