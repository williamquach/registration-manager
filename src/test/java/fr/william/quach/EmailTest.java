package fr.william.quach;

import fr.william.quach.infrastructure.errors.users.InvalidUserGenericException;
import fr.william.quach.domain.models.user.Email;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EmailTest
{
    @Test
    public void should_create_an_email_correctly() throws InvalidUserGenericException {
        String emailAddress = "wquach@myges.fr";
        Email email = Email.create(emailAddress);
        Assertions.assertEquals(emailAddress, email.getEmailAddress());
    }

    @Test
    public void should_fail_when_creating_an_incorrect_email() throws InvalidUserGenericException {
        String emailAddress = "@wrongEmail.fr";

        Assertions.assertThrows(InvalidUserGenericException.class, () -> {
            Email.create(emailAddress);
        });
    }
}
