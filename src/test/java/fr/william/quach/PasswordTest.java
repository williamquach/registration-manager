package fr.william.quach;

import fr.william.quach.infrastructure.errors.users.InvalidUserGenericException;
import fr.william.quach.domain.models.user.Password;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PasswordTest
{
    @Test
    public void should_create_a_password_correctly() throws InvalidUserGenericException {
        String pwd = "myPassword";
        Password password = Password.create(pwd);
        Assertions.assertTrue(password.isSame(pwd));
    }

    @Test
    public void should_fail_when_creating_an_empty_password() throws InvalidUserGenericException {
        String pwd = "";

        Assertions.assertThrows(InvalidUserGenericException.class, () -> {
            Password.create(pwd);
        });
    }
}
