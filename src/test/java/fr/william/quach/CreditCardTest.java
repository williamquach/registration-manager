package fr.william.quach;

import fr.william.quach.infrastructure.errors.payment.PaymentFailedGenericException;
import fr.william.quach.domain.models.payment.CreditCard.CreditCard;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Calendar;

@SpringBootTest
public class CreditCardTest
{
    @Test
    public void should_create_a_credit_card_correctly() {
        String creditCardNumbers = "0000000000000000";
        int expirationMonth = 9;
        int expirationYear = 2022;
        String creditCardCryptogram = "000";

        CreditCard creditCard = CreditCard.create(creditCardNumbers, expirationMonth, expirationYear, creditCardCryptogram);

        Assertions.assertEquals(expirationMonth - 1, creditCard.getExpirationDate().getDate().get(Calendar.MONTH));
        Assertions.assertEquals(expirationYear, creditCard.getExpirationDate().getDate().get(Calendar.YEAR));
        Assertions.assertEquals(creditCardCryptogram, creditCard.getCryptogram());
    }

    @Test
    public void should_throw_exception_since_credit_card_must_contain_sixteen_numbers() {
        LocalDateTime now = LocalDateTime.now();
        String creditCardNumbers = "0";
        int expirationMonth = 9;
        int expirationYear = now.getYear() + 1;
        String creditCardCryptogram = "000";

        Assertions.assertThrows(PaymentFailedGenericException.class, () -> {
            CreditCard creditCard = CreditCard.create(creditCardNumbers, expirationMonth, expirationYear, creditCardCryptogram);
            creditCard.isValidByPrepayment();
        });
    }

    @Test
    public void should_throw_exception_since_credit_card_date_must_be_valid() {
        LocalDateTime now = LocalDateTime.now();
        String creditCardNumbers = "0000000000000000";
        int expirationMonth = 9;
        int expirationYear = now.getYear() - 1;
        String creditCardCryptogram = "000";

        Assertions.assertThrows(PaymentFailedGenericException.class, () -> {
            CreditCard creditCard = CreditCard.create(creditCardNumbers, expirationMonth, expirationYear, creditCardCryptogram);
            creditCard.isValidByPrepayment();
        });
    }

    @Test
    public void should_throw_exception_since_credit_card_cryptogram_must_contain_three_characters() {
        LocalDateTime now = LocalDateTime.now();
        String creditCardNumbers = "0000000000000000";
        int expirationMonth = 9;
        int expirationYear = now.getYear() + 1;
        String creditCardCryptogram = "0";

        Assertions.assertThrows(PaymentFailedGenericException.class, () -> {
            CreditCard creditCard = CreditCard.create(creditCardNumbers, expirationMonth, expirationYear, creditCardCryptogram);
            creditCard.isValidByPrepayment();
        });
    }
}
