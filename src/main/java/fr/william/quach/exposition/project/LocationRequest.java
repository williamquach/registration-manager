package fr.william.quach.exposition.project;

import fr.william.quach.domain.models.project.Location;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class LocationRequest {
    @NotNull @NotBlank
    public String streetNumber;
    @NotNull @NotBlank
    public String streetName;
    @NotNull @NotBlank
    public String city;
    @NotNull @NotBlank
    public String country;

    public LocationRequest(String streetNumber, String streetName, String city, String country) {
        this.streetNumber = streetNumber;
        this.streetName = streetName;
        this.city = city;
        this.country = country;
    }

    public Location toModel() {
        return new Location(streetNumber, streetName, city, country);
    }
}
