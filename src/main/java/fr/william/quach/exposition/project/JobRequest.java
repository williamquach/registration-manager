package fr.william.quach.exposition.project;

import fr.william.quach.domain.models.project.Job;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

public class JobRequest {
    @NotEmpty
    @NotBlank
    public String name;
    public String description;

    public JobRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Job toModel() {
        return new Job(name, description);
    }

    @Override
    public String toString() {
        return "JobRequest{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
