package fr.william.quach.exposition.project;

import fr.william.quach.domain.models.project.Duration;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class DurationRequest {
    @NotNull
    public int numberOfDays;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public LocalDate startDate;

    public DurationRequest(int numberOfDays, LocalDate startDate) {
        this.numberOfDays = numberOfDays;
        this.startDate = startDate;
    }

    public Duration toModel() {
        return new Duration(numberOfDays, startDate);
    }
}
