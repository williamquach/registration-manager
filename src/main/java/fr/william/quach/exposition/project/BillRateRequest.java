package fr.william.quach.exposition.project;

import fr.william.quach.domain.models.project.BillRate;

public class BillRateRequest {
    public float amountPerDay;

    public BillRateRequest() {
    }

    public BillRateRequest(float amountPerDay) {
        this.amountPerDay = amountPerDay;
    }

    public BillRate toModel() {
        return new BillRate(amountPerDay);
    }

    @Override
    public String toString() {
        return "BillRateRequest{" +
                "amountPerDay=" + amountPerDay +
                '}';
    }
}
