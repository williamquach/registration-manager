package fr.william.quach.exposition.project;

import fr.william.quach.domain.models.project.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CreateProjectRequest {
    @NotNull
    @NotBlank
    public final String name;
    @NotEmpty
    public final List<TaskRequest> tasks;
    @NotEmpty
    public final List<SkillsAndBillRateByJobRequest> skillsAndBillRateByJob;
    @NotNull
    @Valid
    public final LocationRequest location;
    @NotNull
    @Valid
    public final DurationRequest duration;

    public CreateProjectRequest(String name, List<TaskRequest> tasks, List<SkillsAndBillRateByJobRequest> skillsAndBillRateByJob, LocationRequest location, DurationRequest duration) {
        this.name = name;
        this.tasks = tasks;
        this.skillsAndBillRateByJob = skillsAndBillRateByJob;
        this.location = location;
        this.duration = duration;
    }

    public List<Task> tasksRequestToModel() {
        return tasks.stream().map(TaskRequest::toModel).collect(Collectors.toList());
    }

    public SkillsAndBillRateByJob skillsByJobRequestToModel() {
        Map<Job, SkillsAndBillRate> jobSkillsAndBillRateHashMap = new HashMap<>();
        for (SkillsAndBillRateByJobRequest skillsAndBillRateByJobRequest : this.skillsAndBillRateByJob) {
            jobSkillsAndBillRateHashMap.put(
                    skillsAndBillRateByJobRequest.job.toModel(),
                    new SkillsAndBillRate(
                            skillsAndBillRateByJobRequest.skills.stream().map(skill ->
                                    new Skill(skill.name, skill.description)
                            ).collect(Collectors.toList()),
                            skillsAndBillRateByJobRequest.billRate.toModel()
                    )
            );
        }
        return new SkillsAndBillRateByJob(jobSkillsAndBillRateHashMap);
    }

    @Override
    public String toString() {
        return "CreateProjectRequest{" +
                "name='" + name + '\'' +
                ", tasks=" + tasks +
                ", skillsAndBillRateByJob=" + skillsAndBillRateByJob +
                ", location=" + location +
                ", duration=" + duration +
                '}';
    }
}

