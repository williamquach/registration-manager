package fr.william.quach.exposition.project;

public class SkillResponse {
    public String name;
    public String description;

    public SkillResponse(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
