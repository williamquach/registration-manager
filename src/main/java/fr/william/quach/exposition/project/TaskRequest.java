package fr.william.quach.exposition.project;

import fr.william.quach.domain.models.project.Task;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class TaskRequest {
    @NotNull @NotBlank
    public String name;
    public String description;
    @NotNull @Valid
    public DurationRequest duration;

    public TaskRequest(String name, String description, DurationRequest duration) {
        this.name = name;
        this.description = description;
        this.duration = duration;
    }

    public Task toModel() {
        return new Task(name, description, duration.toModel());
    }
}
