package fr.william.quach.exposition.project;

public class JobResponse {
    public String name;
    public String description;

    public JobResponse(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
