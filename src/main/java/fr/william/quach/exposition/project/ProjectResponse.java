package fr.william.quach.exposition.project;

import fr.william.quach.domain.enums.ProjectStatus;
import fr.william.quach.domain.models.project.Duration;
import fr.william.quach.domain.models.project.Location;
import fr.william.quach.domain.models.project.ProjectId;
import fr.william.quach.domain.models.project.Task;
import fr.william.quach.domain.models.user.User;

import java.util.List;

public class ProjectResponse {
    private final ProjectId id;
    private final String name;
    private final User leader;
    private final List<User> tradesmen;
    private final List<Task> tasks;
    private final List<SkillsAndBillRateByJobResponse> skillsAndBillRateByJob;
    private final Location location;
    private final Duration duration;
    private final boolean isActive;
    private final ProjectStatus status;

    public ProjectResponse(
            ProjectId id,
            String name,
            User leader,
            List<User> tradesmen,
            List<Task> tasks,
            List<SkillsAndBillRateByJobResponse> skillsAndBillRateByJob,
            Location location,
            Duration duration,
            boolean isActive,
            ProjectStatus status
    ) {
        this.id = id;
        this.name = name;
        this.leader = leader;
        this.tradesmen = tradesmen;
        this.tasks = tasks;
        this.skillsAndBillRateByJob = skillsAndBillRateByJob;
        this.location = location;
        this.duration = duration;
        this.isActive = isActive;
        this.status = status;
    }

    public ProjectId getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public User getLeader() {
        return leader;
    }

    public List<User> getTradesmen() {
        return tradesmen;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public List<SkillsAndBillRateByJobResponse> getSkillsAndBillRateByJob() {
        return skillsAndBillRateByJob;
    }

    public Location getLocation() {
        return location;
    }

    public Duration getDuration() {
        return duration;
    }

    public boolean isActive() {
        return isActive;
    }

    public ProjectStatus getStatus() {
        return status;
    }
}

