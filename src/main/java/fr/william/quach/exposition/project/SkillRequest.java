package fr.william.quach.exposition.project;

import fr.william.quach.domain.models.project.Skill;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class SkillRequest {
    @NotNull @NotBlank
    public String name;
    public String description;

    public SkillRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Skill toModel() {
        return new Skill(name, description);
    }
}
