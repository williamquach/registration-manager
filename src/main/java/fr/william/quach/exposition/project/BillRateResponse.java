package fr.william.quach.exposition.project;

public class BillRateResponse {
    public float amountPerDay;

    public BillRateResponse(float amountPerDay) {
        this.amountPerDay = amountPerDay;
    }
}
