package fr.william.quach.exposition.project;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

public class SkillsAndBillRateByJobRequest {
    @Valid
    public JobRequest job;
    @NotEmpty
    public List<SkillRequest> skills;
    @Valid
    public BillRateRequest billRate;

    public SkillsAndBillRateByJobRequest(JobRequest job, List<SkillRequest> skills, BillRateRequest billRate) {
        this.job = job;
        this.skills = skills;
        this.billRate = billRate;
    }

    @Override
    public String toString() {
        return "SkillsAndBillRateByJobRequest{" +
                "job=" + job +
                ", skills=" + skills +
                ", billRate=" + billRate +
                '}';
    }
}
