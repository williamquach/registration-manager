package fr.william.quach.exposition.project;

import java.util.List;

public class SkillsAndBillRateByJobResponse {
    public JobResponse job;
    public List<SkillResponse> skills;
    public BillRateResponse billRate;

    public SkillsAndBillRateByJobResponse(JobResponse job, List<SkillResponse> skills, BillRateResponse billRate) {
        this.job = job;
        this.skills = skills;
        this.billRate = billRate;
    }
}
