package fr.william.quach.exposition.user;

import fr.william.quach.domain.models.user.UserId;

public class UserResponse {
    public UserId userId;
    public String firstname;
    public String lastname;
    public EmailRequest email;

    public UserResponse() {}

    public UserResponse(UserId userId, String lastname, String firstname, EmailRequest email) {
        this.userId = userId;
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "userId=" + userId +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email=" + email +
                '}';
    }
}
