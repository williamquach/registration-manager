package fr.william.quach.exposition.user;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PasswordRequest {
    @NotNull
    @NotBlank
    public String password;

    public PasswordRequest() {
    }

    public PasswordRequest(String password) {
        this.password = password;
    }
}
