package fr.william.quach.exposition.user;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class UsersResponse {
    public List<UserResponse> users;

    public UsersResponse() {
        this.users = new ArrayList<>();
    }

    public UsersResponse(List<UserResponse> users) {
        this.users = users;
    }
}
