package fr.william.quach.exposition.user;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CreateTradesmanRequest {

    @NotNull
    @NotBlank
    public String firstname;
    @NotNull
    @NotBlank
    public String lastname;
    @Valid
    public EmailRequest email;
    @Valid
    public PasswordRequest password;
    public PaymentMethodRequest paymentMethod;


    public CreateTradesmanRequest(String lastname, String firstname, EmailRequest emailAddress, PasswordRequest password, PaymentMethodRequest paymentMethod) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = emailAddress;
        this.password = password;
        this.paymentMethod = paymentMethod;
    }

    @Override
    public String toString() {
        return "CreateTradesmanRequest{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email=" + email +
                ", password=" + password +
                ", paymentMethod=" + paymentMethod +
                '}';
    }
}
