package fr.william.quach.exposition.user;

import javax.validation.constraints.Email;

public class EmailRequest {
    @Email
    public String emailAddress;

    public EmailRequest() {
    }

    public EmailRequest(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
