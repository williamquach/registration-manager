package fr.william.quach.exposition.user;

public class EmailResponse {

    public final String emailAddress;

    public EmailResponse(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "EmailResponse{" +
                "emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
