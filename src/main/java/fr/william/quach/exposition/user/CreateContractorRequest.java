package fr.william.quach.exposition.user;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CreateContractorRequest {

    @NotNull
    @NotBlank
    public String firstname;
    @NotNull
    @NotBlank
    public String lastname;
    @Valid
    public EmailRequest email;
    @Valid
    public PasswordRequest password;
    public PaymentMethodRequest paymentMethod;

    public CreateContractorRequest(String firstname, String lastname, EmailRequest emailRequest, PasswordRequest password, PaymentMethodRequest paymentMethod) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = emailRequest;
        this.password = password;
        this.paymentMethod = paymentMethod;
    }

    @Override
    public String toString() {
        return "CreateContractorRequest{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", emailRequest=" + email +
                ", password=" + password +
                ", creditCard=" + paymentMethod +
                '}';
    }
}
