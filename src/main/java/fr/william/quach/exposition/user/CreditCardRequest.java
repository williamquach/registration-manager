package fr.william.quach.exposition.user;

public class CreditCardRequest implements PaymentMethodRequest {
    public String creditCardNumbers;
    public int expirationMonth;
    public int expirationYear;
    public String cryptogram;

    public CreditCardRequest(String creditCardNumbers, int expirationMonth, int expirationYear, String cryptogram) {
        this.creditCardNumbers = creditCardNumbers;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
        this.cryptogram = cryptogram;
    }

    @Override
    public String toString() {
        return "CreditCardRequest{" +
                "creditCardNumbers=" + creditCardNumbers + '\'' +
                ", expirationMonth=" + expirationMonth +
                ", expirationYear=" + expirationYear +
                ", cryptogram='" + cryptogram + '\'' +
                '}';
    }
}
