package fr.william.quach.exposition.controllers;

import fr.william.quach.application.user.ApplyForMemberShip;
import fr.william.quach.application.user.RetrieveUsers;
import fr.william.quach.domain.models.payment.PaymentMethod;
import fr.william.quach.domain.models.user.*;
import fr.william.quach.exposition.user.*;
import fr.william.quach.kernel.command.CommandBus;
import fr.william.quach.kernel.query.QueryBus;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    private final QueryBus queryBus;
    private final CommandBus commandBus;

    public UserController(@Qualifier("userQueryBus") QueryBus queryBus, @Qualifier("userCommandBus") CommandBus commandBus) {
        this.queryBus = queryBus;
        this.commandBus = commandBus;
    }

    @GetMapping(value = "/users", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UsersResponse> getAll() {
        UsersResponse usersResponseResult = queryBus.send(new RetrieveUsers());
        return ResponseEntity.ok(usersResponseResult);
    }

    /**
     * Example of request :
     * POST http://localhost:8080/apply-membership/tradesman
     * With body :
     * {
     * "firstname": "Jean",
     * "lastname": "DUPONT",
     * "email": {
     * "emailAddress": "jdupont@myges.fr"
     * },
     * "password": {
     * "password": "password123"
     * },
     * "creditCard": {
     * "creditCardNumbers": "0000111122223333",
     * "expirationMonth": 1,
     * "expirationYear": 2030,
     * "cryptogram": "123"
     * //TODO Tradesman attributes
     * }
     * }
     *
     * @param newTradesman who applies membership
     * @return new user id
     * @throws Exception if cannot get application context
     */
    @PostMapping(value = "/apply-membership/tradesman")
    public ResponseEntity<UserResponse> applyTradesmanMembership(@RequestBody CreateTradesmanRequest newTradesman) {
        ApplyForMemberShip newMemberShip = new ApplyForMemberShip(
                newTradesman.lastname,
                newTradesman.firstname,
                Email.create(newTradesman.email.emailAddress),
                Password.create(newTradesman.password.password),
                PaymentMethod.getPaymentMethodModelFromRequest(newTradesman.paymentMethod),
                new Tradesman()
        );
        final User user = commandBus.send(newMemberShip);
        UserResponse userResponse = new UserResponse(
                user.getUserId(), user.getFirstname(), user.getLastname(), new EmailRequest(user.getEmailAddress())
        );
        return ResponseEntity.ok(userResponse);
    }

    /**
     * Example of request :
     * POST http://localhost:8080/apply-membership/contractor
     * With body :
     * {
     * "firstname": "Jean",
     * "lastname": "DUPONT",
     * "email": {
     * "emailAddress": "jdupont@myges.fr"
     * },
     * "password": {
     * "password": "password123"
     * },
     * "creditCard": {
     * "creditCardNumbers": "0000111122223333",
     * "expirationMonth": 1,
     * "expirationYear": 2030,
     * "cryptogram": "123"
     * //TODO Contractor attributes
     * }
     * }
     *
     * @param newContractor who applies membership
     * @return new user id
     * @throws Exception if cannot get application context
     */
    @PostMapping(value = "/apply-membership/contractor")
    public ResponseEntity<UserResponse> applyContractorMembership(@RequestBody CreateContractorRequest newContractor) {
        ApplyForMemberShip newMemberShip = new ApplyForMemberShip(newContractor.lastname,
                newContractor.firstname,
                Email.create(newContractor.email.emailAddress),
                Password.create(newContractor.password.password),
                PaymentMethod.getPaymentMethodModelFromRequest(newContractor.paymentMethod),
                new Contractor()
        );
        final User user = commandBus.send(newMemberShip);
        UserResponse userResponse = new UserResponse(
                user.getUserId(), user.getFirstname(), user.getLastname(), new EmailRequest(user.getEmailAddress())
        );
        return ResponseEntity.ok(userResponse);
    }
}
