package fr.william.quach.exposition.controllers;

import fr.william.quach.application.project.AssignTradesmanToProject;
import fr.william.quach.application.project.CreateProject;
import fr.william.quach.application.project.GetProjectBestTradesmanMatch;
import fr.william.quach.application.project.UpdateProjectStatus;
import fr.william.quach.domain.enums.ProjectStatus;
import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.exposition.project.CreateProjectRequest;
import fr.william.quach.exposition.project.ProjectResponse;
import fr.william.quach.exposition.user.EmailRequest;
import fr.william.quach.exposition.user.UserResponse;
import fr.william.quach.kernel.command.CommandBus;
import fr.william.quach.kernel.query.QueryBus;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@RestController
public class ProjectController {
    private final QueryBus queryBus;
    private final CommandBus commandBus;

    public ProjectController(@Qualifier("projectQueryBus") QueryBus queryBus, @Qualifier("projectCommandBus") CommandBus commandBus) {
        this.queryBus = queryBus;
        this.commandBus = commandBus;
    }

    // Use case 7
    @PostMapping(path = "/user/{leaderId}/project", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProjectResponse> createProject(
            @RequestBody @Valid CreateProjectRequest createProjectRequest,
            @PathVariable String leaderId
    ) {
        CreateProject createProject = new CreateProject(
                createProjectRequest.name,
                leaderId,
                createProjectRequest.tasksRequestToModel(),
                createProjectRequest.skillsByJobRequestToModel(),
                createProjectRequest.location.toModel(),
                createProjectRequest.duration.toModel()
        );
        final Project project = commandBus.send(createProject);
        return ResponseEntity.ok().body(project.toResponse());
    }

    // Use case 2
    @PostMapping(path = "/user/{leaderId}/project/{projectId}/request-tradesman", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Project> requestTradesmanInProject(
            @PathVariable String projectId,
            @PathVariable String leaderId // TODO: Middleware handling authorization
    ) {
        UpdateProjectStatus createProject = new UpdateProjectStatus(projectId, ProjectStatus.SEARCHING_TRADESMAN);
        final Project project = commandBus.send(createProject);
        return ResponseEntity.ok().body(project);
    }

    // Use case 3
    @GetMapping(path = "/user/{leaderId}/project/{projectId}/best-tradesman", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> getProjectBestTradesmanMatch(
            @PathVariable String projectId,
            @PathVariable String leaderId // TODO: Middleware handling authorization
    ) {
        GetProjectBestTradesmanMatch getProjectBestTradesmanMatch = new GetProjectBestTradesmanMatch(projectId);
        final User user = queryBus.send(getProjectBestTradesmanMatch);
        UserResponse userResponse = new UserResponse(
                user.getUserId(), user.getFirstname(), user.getLastname(), new EmailRequest(user.getEmailAddress())
        );
        return ResponseEntity.ok().body(userResponse);
    }

    // Use case 4
    @PostMapping(path = "/user/{leaderId}/project/{projectId}/assign/{tradesmanId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Project> assignTradesmanToProject(
            @PathVariable String projectId,
            @PathVariable String tradesmanId,
            @PathVariable String leaderId // TODO: Middleware handling authorization
    ) {
        AssignTradesmanToProject assignTradesmanToProject = new AssignTradesmanToProject(projectId, tradesmanId);
        final Project project = commandBus.send(assignTradesmanToProject);
        return ResponseEntity.ok().body(project);
    }
}
