package fr.william.quach.application.user;

import fr.william.quach.domain.models.payment.Amount;
import fr.william.quach.domain.models.payment.Currency;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.infrastructure.errors.payment.PaymentFailedGenericException;
import fr.william.quach.infrastructure.errors.users.UserMembershipGenericException;
import fr.william.quach.infrastructure.services.Scheduler.PaymentSchedulerService;
import fr.william.quach.infrastructure.services.Scheduler.SchedulerService;
import fr.william.quach.infrastructure.services.UserPaymentProcessor;
import fr.william.quach.kernel.Subscriber;

public class RegisterUserEventSubscription implements Subscriber<RegisterUserEvent> {

    private final Amount FIXED_MONTHLY_PAYMENT_AMOUNT = new Amount(14.99, new Currency("France", "Euro", "€", "EUR"));

    private final UserPaymentProcessor userPaymentProcessor;

    public RegisterUserEventSubscription(
            UserPaymentProcessor userPaymentProcessor
    ) {
        this.userPaymentProcessor = userPaymentProcessor;
    }

    @Override
    public void accept(RegisterUserEvent event) {
        User user = event.getUser();
        try {
            userPaymentProcessor.processPayment(
                    new Amount(0, new Currency("France", "euro", "€", "EUR")),
                    user
            );
            PaymentSchedulerService paymentSchedulerService = new PaymentSchedulerService(new SchedulerService());
            paymentSchedulerService.launchMonthlyPayment(user, FIXED_MONTHLY_PAYMENT_AMOUNT);
        } catch (InterruptedException e) {
            throw new PaymentFailedGenericException("Cannot register user %s because of PaymentScheduler failed", e);
        } catch (UserMembershipGenericException | PaymentFailedGenericException e) {
            try {
                throw UserMembershipGenericException.prepaymentFailed(String.format("Cannot register user %s because of prepayment failed", user), e);
            } catch (UserMembershipGenericException ex) {
                ex.printStackTrace();
            }
        }
    }
}
