package fr.william.quach.application.user;

import fr.william.quach.domain.models.payment.PaymentMethod;
import fr.william.quach.domain.models.user.Email;
import fr.william.quach.domain.models.user.Password;
import fr.william.quach.domain.models.user.UserRole;
import fr.william.quach.kernel.command.Command;

/**
 * Command object
 */
public final class ApplyForMemberShip implements Command {
    public final String lastname;
    public final String firstname;
    public final Email email;
    public final Password password;
    public final PaymentMethod paymentMethod;
    public final UserRole role;

    public ApplyForMemberShip(
            String lastname,
            String firstname,
            Email email,
            Password password,
            PaymentMethod paymentMethod,
            UserRole role
    ) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
        this.password = password;
        this.paymentMethod = paymentMethod;
        this.role = role;
    }

}
