package fr.william.quach.application.user;


import fr.william.quach.kernel.event.Event;
import fr.william.quach.kernel.event.EventId;
import fr.william.quach.domain.models.user.User;

import java.time.LocalDateTime;

public class RegisterUserEvent implements Event {

    private final EventId eventId;
    private final LocalDateTime occurredDate;
    private final User user;

    private RegisterUserEvent(EventId eventId, LocalDateTime occurredDate, User person) {
        this.eventId = eventId;
        this.occurredDate = occurredDate;
        this.user = person;
    }

    public static RegisterUserEvent withUser(User person) {
        return new RegisterUserEvent(EventId.create(), LocalDateTime.now(), person);
    }

    @Override
    public EventId getId() {
        return eventId;
    }

    @Override
    public LocalDateTime getOccurredDate() {
        return occurredDate;
    }

    public EventId getEventId() {
        return eventId;
    }

    public User getUser() {
        return user;
    }
}
