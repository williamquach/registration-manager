package fr.william.quach.application.user;

import fr.william.quach.kernel.query.QueryHandler;
import fr.william.quach.domain.repositories.UserRepository;
import fr.william.quach.exposition.user.UserResponse;
import fr.william.quach.exposition.user.EmailRequest;

import java.util.List;
import java.util.stream.Collectors;

public class RetrieveUsersHandler implements QueryHandler<RetrieveUsers, List<UserResponse>> {

    private final UserRepository userRepository;

    public RetrieveUsersHandler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserResponse> handle(RetrieveUsers query) {
        return userRepository.findAll()
                .stream()
                .map(user ->
                        new UserResponse(
                                user.getUserId(),
                                user.getLastname(), user.getFirstname(),
                                new EmailRequest(user.getEmailAddress())
                        )
                )
                .collect(Collectors.toList());
    }
}
