package fr.william.quach.application.user;

import fr.william.quach.domain.models.user.Contractor;
import fr.william.quach.domain.models.user.Tradesman;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.domain.models.user.UserId;
import fr.william.quach.domain.repositories.UserRepository;
import fr.william.quach.kernel.command.CommandHandler;

public final class ApplyForMemberShipCommandHandler implements CommandHandler<ApplyForMemberShip, User> {

    private final UserRepository userRepository;

    public ApplyForMemberShipCommandHandler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User handle(ApplyForMemberShip createUser) {
        final UserId userId = userRepository.nextId();
        User user = null;
        if (createUser.role instanceof Contractor) {
            user = User.create(
                    userId,
                    createUser.firstname,
                    createUser.lastname,
                    createUser.email.getEmailAddress(),
                    createUser.password.getPassword(),
                    createUser.paymentMethod,
                    new Contractor()
            );
        } else {
            user = User.create(
                    userId,
                    createUser.firstname,
                    createUser.lastname,
                    createUser.email.getEmailAddress(),
                    createUser.password.getPassword(),
                    createUser.paymentMethod,
                    new Tradesman()
            );
        }
        userRepository.doesUserAlreadyExist(user);
        User newUser = userRepository.applyForMembership(user);
        System.out.println("=======");
        System.out.printf("%s is now a member\n", newUser);
        System.out.println("=======");
        return newUser;
    }
}
