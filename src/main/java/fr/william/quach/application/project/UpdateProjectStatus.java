package fr.william.quach.application.project;

import fr.william.quach.domain.enums.ProjectStatus;
import fr.william.quach.domain.models.project.*;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.kernel.command.Command;

import java.util.ArrayList;
import java.util.List;

public class UpdateProjectStatus implements Command {
    public final String projectId;
    public final ProjectStatus projectStatus;

    public UpdateProjectStatus(String projectId, ProjectStatus projectStatus) {
        this.projectId = projectId;
        this.projectStatus = projectStatus;
    }
}
