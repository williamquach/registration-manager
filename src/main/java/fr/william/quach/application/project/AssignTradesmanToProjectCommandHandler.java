package fr.william.quach.application.project;

import fr.william.quach.domain.enums.ProjectStatus;
import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.project.ProjectId;
import fr.william.quach.domain.models.user.Tradesman;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.domain.models.user.UserId;
import fr.william.quach.domain.repositories.ProjectRepository;
import fr.william.quach.domain.repositories.UserRepository;
import fr.william.quach.infrastructure.errors.project.ProjectNotFoundException;
import fr.william.quach.infrastructure.errors.users.UserException;
import fr.william.quach.infrastructure.errors.users.UserNotFoundException;
import fr.william.quach.kernel.command.CommandHandler;

import java.util.Optional;

public final class AssignTradesmanToProjectCommandHandler implements CommandHandler<AssignTradesmanToProject, Project> {

    private final UserRepository userRepository;
    private final ProjectRepository projectRepository;

    public AssignTradesmanToProjectCommandHandler(UserRepository userRepository, ProjectRepository projectRepository) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Project handle(AssignTradesmanToProject assignTradesmanToProject) {
        final Optional<User> optionalUser = userRepository.findById(UserId.of(assignTradesmanToProject.tradesmanId));
        if (optionalUser.isEmpty()) {
            throw new UserNotFoundException(UserId.of(assignTradesmanToProject.tradesmanId));
        }
        final Optional<Project> optionalProject = projectRepository.findById(ProjectId.of(assignTradesmanToProject.projectId));
        if (optionalProject.isEmpty()) {
            throw new ProjectNotFoundException(assignTradesmanToProject.projectId);
        }

        Project project = optionalProject.get();
        User user = optionalUser.get();
        Tradesman userRole = ((Tradesman) user.getRole());
        if (!userRole.isAvailable()) {
            throw new UserException(String.format("User %s is not available", user.getFullName()));
        }

        userRole.makeOccupied();
        project.addTradesman(user);
        project.updateStatus(ProjectStatus.TRADESMAN_ASSIGNED);

        projectRepository.register(project);

        System.out.println("=======");
        System.out.printf("Add tradesman %s to project : '%s'\n", user.getFullName(), project.getName());
        System.out.println("=======");

        return project;
    }
}
