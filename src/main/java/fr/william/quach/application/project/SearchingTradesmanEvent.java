package fr.william.quach.application.project;


import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.kernel.event.Event;
import fr.william.quach.kernel.event.EventId;

import java.time.LocalDateTime;

public class SearchingTradesmanEvent implements Event {

    private final EventId eventId;
    private final LocalDateTime occurredDate;
    private final Project project;

    private SearchingTradesmanEvent(EventId eventId, LocalDateTime occurredDate, Project project) {
        this.eventId = eventId;
        this.occurredDate = occurredDate;
        this.project = project;
    }

    public static SearchingTradesmanEvent withProject(Project project) {
        return new SearchingTradesmanEvent(EventId.create(), LocalDateTime.now(), project);
    }

    @Override
    public EventId getId() {
        return eventId;
    }

    @Override
    public LocalDateTime getOccurredDate() {
        return occurredDate;
    }

    public EventId getEventId() {
        return eventId;
    }

    public Project getProject() {
        return project;
    }
}
