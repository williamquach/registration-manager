package fr.william.quach.application.project;

import fr.william.quach.domain.enums.ProjectStatus;
import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.domain.repositories.ProjectRepository;
import fr.william.quach.domain.repositories.UserRepository;
import fr.william.quach.infrastructure.errors.users.UserNotFoundException;
import fr.william.quach.kernel.Subscriber;

import java.util.List;

public class SearchingTradesmanEventSubscription implements Subscriber<SearchingTradesmanEvent> {

    private UserRepository userRepository;
    private ProjectRepository projectRepository;

    public SearchingTradesmanEventSubscription(UserRepository userRepository, ProjectRepository projectRepository) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
    }

    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void accept(SearchingTradesmanEvent event) {
        Project project = event.getProject();
        List<User> tradesmen = this.userRepository.findAvailableTradesmen();
        if (tradesmen.isEmpty()) {
            throw new UserNotFoundException("No available tradesmen found");
        }
        User bestTradesman = project.matchNeeds(tradesmen);
        if (bestTradesman != null) {
            project.setBestTradesmanMatch(bestTradesman);
            project.updateStatus(ProjectStatus.FOUND_TRADESMAN);
            projectRepository.register(project);
        }
        // possible envoi d'email au contractor du projet
    }
}
