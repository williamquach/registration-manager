package fr.william.quach.application.project;

import fr.william.quach.kernel.command.Command;

public class AssignTradesmanToProject implements Command {
    public final String projectId;
    public final String tradesmanId;

    public AssignTradesmanToProject(String projectId, String tradesmanId) {
        this.projectId = projectId;
        this.tradesmanId = tradesmanId;
    }
}
