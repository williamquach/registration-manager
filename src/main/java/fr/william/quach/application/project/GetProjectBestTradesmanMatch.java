package fr.william.quach.application.project;

import fr.william.quach.kernel.query.Query;

public class GetProjectBestTradesmanMatch implements Query {
    public final String projectId;

    public GetProjectBestTradesmanMatch(String projectId) {
        this.projectId = projectId;
    }
}
