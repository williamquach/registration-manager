package fr.william.quach.application.project;

import fr.william.quach.domain.enums.ProjectStatus;
import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.project.ProjectId;
import fr.william.quach.domain.repositories.ProjectRepository;
import fr.william.quach.infrastructure.errors.project.ProjectException;
import fr.william.quach.infrastructure.errors.project.ProjectNotActiveException;
import fr.william.quach.infrastructure.errors.project.ProjectNotFoundException;
import fr.william.quach.kernel.command.CommandHandler;

import java.util.Optional;

public final class UpdateProjectStatusCommandHandler implements CommandHandler<UpdateProjectStatus, Project> {

    private final ProjectRepository projectRepository;

    public UpdateProjectStatusCommandHandler(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project handle(UpdateProjectStatus updateProjectStatus) {
        final Optional<Project> getProject = projectRepository.findById(ProjectId.of(updateProjectStatus.projectId));
        if (getProject.isEmpty()) {
            throw new ProjectNotFoundException(updateProjectStatus.projectId);
        }

        Project project = getProject.get();

        if (!project.isActive()) {
            throw new ProjectNotActiveException(project);
        }
        if (project.getStatus() != ProjectStatus.NO_TRADESMAN) {
            throw new ProjectException("Project cannot search for tradesman because it has already started searching or found tradesman");
        }
        project.updateStatus(ProjectStatus.SEARCHING_TRADESMAN);
        projectRepository.updateProjectStatus(project);
        System.out.println("=======");
        System.out.printf("Project updated : '%s' is now '%s'\n", project, project.getStatus());
        System.out.println("=======");
        return project;
    }
}
