package fr.william.quach.application.project;

import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.project.ProjectId;
import fr.william.quach.domain.models.user.Contractor;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.domain.models.user.UserId;
import fr.william.quach.domain.repositories.ProjectRepository;
import fr.william.quach.domain.repositories.UserRepository;
import fr.william.quach.infrastructure.errors.project.ProjectException;
import fr.william.quach.infrastructure.errors.users.UserNotFoundException;
import fr.william.quach.kernel.command.CommandHandler;

import java.util.Optional;

public final class CreateProjectCommandHandler implements CommandHandler<CreateProject, Project> {

    private final UserRepository userRepository;
    private final ProjectRepository projectRepository;

    public CreateProjectCommandHandler(UserRepository userRepository, ProjectRepository projectRepository) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Project handle(CreateProject createProject) {
        final ProjectId projectId = projectRepository.nextId();
        final Optional<User> optionalLeader = userRepository.findById(UserId.of(createProject.leaderId));
        if (optionalLeader.isEmpty()) {
            throw new UserNotFoundException(UserId.of(createProject.leaderId));
        }
        final User leader = optionalLeader.get();
        if (!(leader.getRole() instanceof Contractor)) {
            throw new ProjectException("Project leader can only be a contractor!");
        }
        Project project = new Project(projectId, createProject.name, leader, createProject.status);
        project.setTasks(createProject.tasks);
        project.setSkillsAndBillRateByJob(createProject.skillsAndBillRateByJob);
        project.setLocation(createProject.location);
        project.setDuration(createProject.duration);

        projectRepository.createProject(project);
        System.out.println("=======");
        System.out.printf("Project : '%s' created\n", project);
        System.out.println("=======");
        return project;
    }
}
