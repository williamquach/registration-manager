package fr.william.quach.application.project;

import fr.william.quach.domain.enums.ProjectStatus;
import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.project.ProjectId;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.domain.repositories.ProjectRepository;
import fr.william.quach.infrastructure.errors.project.ProjectException;
import fr.william.quach.infrastructure.errors.project.ProjectNotFoundException;
import fr.william.quach.kernel.query.QueryHandler;

import java.util.Optional;

public final class GetProjectBestTradesmanMatchQueryHandler implements QueryHandler<GetProjectBestTradesmanMatch, User> {

    private final ProjectRepository projectRepository;

    public GetProjectBestTradesmanMatchQueryHandler(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public User handle(GetProjectBestTradesmanMatch getProjectBestTradesmanMatch) {
        final Optional<Project> getProject = projectRepository.findById(ProjectId.of(getProjectBestTradesmanMatch.projectId));
        if (getProject.isEmpty()) {
            throw new ProjectNotFoundException(getProjectBestTradesmanMatch.projectId);
        }

        Project project = getProject.get();
        if (project.getStatus() != ProjectStatus.FOUND_TRADESMAN && project.getBestTradesmanMatch() == null) {
            throw new ProjectException("Cannot get project best tradesman. No match yet or search not started.");
        }

        return project.getBestTradesmanMatch();
    }
}
