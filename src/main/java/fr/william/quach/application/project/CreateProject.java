package fr.william.quach.application.project;

import fr.william.quach.domain.enums.ProjectStatus;
import fr.william.quach.domain.models.project.Duration;
import fr.william.quach.domain.models.project.Location;
import fr.william.quach.domain.models.project.SkillsAndBillRateByJob;
import fr.william.quach.domain.models.project.Task;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.kernel.command.Command;

import java.util.ArrayList;
import java.util.List;

public class CreateProject implements Command {
    public final String name;
    public final String leaderId;
    public final List<User> tradesmen;
    public final List<Task> tasks;
    public final SkillsAndBillRateByJob skillsAndBillRateByJob;
    public final Location location;
    public final Duration duration;
    public final boolean isActive;
    public final ProjectStatus status;

    public CreateProject(String name, String leaderId, List<Task> tasks, SkillsAndBillRateByJob skillsAndBillRateByJob, Location location, Duration duration) {
        this.name = name;
        this.leaderId = leaderId;
        this.tradesmen = new ArrayList<>();
        this.tasks = tasks;
        this.skillsAndBillRateByJob = skillsAndBillRateByJob;
        this.location = location;
        this.duration = duration;
        this.isActive = true;
        this.status = ProjectStatus.NO_TRADESMAN;
    }
}
