package fr.william.quach;

import fr.william.quach.application.user.ApplyForMemberShip;
import fr.william.quach.application.user.ApplyForMemberShipCommandHandler;
import fr.william.quach.application.user.RetrieveUsers;
import fr.william.quach.application.user.RetrieveUsersHandler;
import fr.william.quach.domain.repositories.UserRepository;
import fr.william.quach.infrastructure.repositories.user.InMemoryUserRepository;
import fr.william.quach.kernel.command.Command;
import fr.william.quach.kernel.command.CommandBus;
import fr.william.quach.kernel.command.CommandHandler;
import fr.william.quach.kernel.command.SimpleCommandBus;
import fr.william.quach.kernel.query.Query;
import fr.william.quach.kernel.query.QueryBus;
import fr.william.quach.kernel.query.QueryHandler;
import fr.william.quach.kernel.query.SimpleQueryBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.Map;

@Configuration
public class UserConfiguration {
    @Bean
    public static UserRepository userRepository() {
        return InMemoryUserRepository.getInstance();
    }

    @Bean
    public ApplyForMemberShipCommandHandler createApplyForMemberShipCommandHandler() {
        return new ApplyForMemberShipCommandHandler(userRepository());
    }

    @Bean
    public RetrieveUsersHandler retrieveUsersHandler() {
        return new RetrieveUsersHandler(userRepository());
    }

    @Bean
    public QueryBus userQueryBus() {
        final Map<Class<? extends Query>, QueryHandler> queryHandlerMap =
                Collections.singletonMap(RetrieveUsers.class, new RetrieveUsersHandler(userRepository()));
        return new SimpleQueryBus(queryHandlerMap);
    }

    @Bean
    public CommandBus userCommandBus() {
        final Map<Class<? extends Command>, CommandHandler> commandHandlerMap =
                Collections.singletonMap(ApplyForMemberShip.class, new ApplyForMemberShipCommandHandler(userRepository()));
        return new SimpleCommandBus(commandHandlerMap);
    }
}
