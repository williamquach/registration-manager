package fr.william.quach.infrastructure.repositories.database;

import java.sql.*;

public class DBConnection {
    private final String driver;
    private final String host;
    private final String port;
    private final String username;
    private final String password;
    private Connection connection;

    public DBConnection(String driver, String host, String port, String username, String password) {
        this.driver = driver;
        this.host = host; // e.g, "jdbc:oracle:thin:@localhost:1521:xe"
        this.port = port;
        this.username = username;
        this.password = password;
    }

    public void getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(this.driver);
        String url = String.format("%s:@%s:%s:database", driver, host, port);
        this.connection = DriverManager.getConnection(url, this.username, this.password);
    }

    public int executeUpdate(String query) throws SQLException {
        return 1; // this.createStatement().executeUpdate(query);
    }

    private ResultSet executeQuery(String query) throws SQLException {
        if (this.connection != null) {
            return this.connection.createStatement().executeQuery(query);
        }
        return null;
    }

    public void close() throws SQLException {
        if (this.connection != null) {
            this.connection.close();
        }
    }
}
