package fr.william.quach.infrastructure.repositories.user;

import fr.william.quach.application.user.RegisterUserEvent;
import fr.william.quach.application.user.RegisterUserEventSubscription;
import fr.william.quach.domain.models.user.Tradesman;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.domain.models.user.UserId;
import fr.william.quach.domain.repositories.UserRepository;
import fr.william.quach.infrastructure.errors.users.InvalidUserGenericException;
import fr.william.quach.infrastructure.errors.users.UserAlreadyExistException;
import fr.william.quach.infrastructure.errors.users.UserMembershipGenericException;
import fr.william.quach.infrastructure.errors.users.UserRegistrationException;
import fr.william.quach.infrastructure.services.UserPaymentProcessor;
import fr.william.quach.kernel.event.DefaultEventBus;
import fr.william.quach.kernel.event.Event;
import fr.william.quach.kernel.event.EventBus;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class InMemoryUserRepository implements UserRepository {

    private static InMemoryUserRepository userRepository;

    private final AtomicInteger counter = new AtomicInteger(0);
    private final Map<UserId, User> data = new ConcurrentHashMap<>();

    private final EventBus<Event> eventBus;

    private InMemoryUserRepository(EventBus<Event> eventBus) {
        this.eventBus = eventBus;
    }

    public static InMemoryUserRepository getInstance() {
        if (userRepository == null) {
            Map<Class<RegisterUserEvent>, List<RegisterUserEventSubscription>> subscriptionMap =
                    Collections.singletonMap(
                            RegisterUserEvent.class,
                            Collections.singletonList(
                                    new RegisterUserEventSubscription(new UserPaymentProcessor())
                            )
                    );
            EventBus eventBus = new DefaultEventBus(subscriptionMap);
            userRepository = new InMemoryUserRepository(eventBus);
        }
        return userRepository;
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(data.values());
    }

    @Override
    public List<User> findAvailableTradesmen() {
        return data.values().stream().filter(user -> user.getRole() instanceof Tradesman).collect(Collectors.toList());
    }

    @Override
    public Optional<User> findById(UserId id) {
        return Optional.ofNullable(data.get(id));
    }

    @Override
    public boolean doesUserAlreadyExist(User user) throws UserAlreadyExistException {
        Optional<User> userExistByEmail = data.values().stream().filter((currentUser) -> Objects.equals(currentUser.getEmailAddress(), user.getEmailAddress())).findAny();
        if (userExistByEmail.isPresent()) {
            throw new UserAlreadyExistException("User with email : '" + user.getEmailAddress() + "' already exist");
        }
        return false;
    }

    @Override
    public User applyForMembership(User user) throws UserMembershipGenericException {
        try {
            System.out.println("============================================");
            System.out.println("New membership application from : " + user);
            System.out.println("============================================\n");
            doesUserAlreadyExist(user);
            eventBus.send(RegisterUserEvent.withUser(user));
            return register(user);
        } catch (UserRegistrationException | InvalidUserGenericException e) {
            throw UserMembershipGenericException.membershipApplicationFailed("User data are not valid, we cannot accept his application.", e);
        } catch (Exception e) {
            throw UserMembershipGenericException.membershipApplicationFailed("User payment method is invalid, we cannot accept his application.", e);
        }
    }

    @Override
    public User register(User user) {
        data.put(user.getUserId(), user);
        return user;
    }

    @Override
    public UserId nextId() {
        return UserId.of(String.valueOf(counter.incrementAndGet()));
    }
}
