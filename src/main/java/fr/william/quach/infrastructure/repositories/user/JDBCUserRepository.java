package fr.william.quach.infrastructure.repositories.user;

import fr.william.quach.application.user.RegisterUserEvent;
import fr.william.quach.application.user.RegisterUserEventSubscription;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.domain.models.user.UserId;
import fr.william.quach.domain.repositories.UserRepository;
import fr.william.quach.infrastructure.errors.users.InvalidUserGenericException;
import fr.william.quach.infrastructure.errors.users.UserAlreadyExistException;
import fr.william.quach.infrastructure.errors.users.UserMembershipGenericException;
import fr.william.quach.infrastructure.errors.users.UserRegistrationException;
import fr.william.quach.infrastructure.repositories.database.DBConnection;
import fr.william.quach.infrastructure.services.UserPaymentProcessor;
import fr.william.quach.kernel.event.DefaultEventBus;
import fr.william.quach.kernel.event.Event;
import fr.william.quach.kernel.event.EventBus;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class JDBCUserRepository implements UserRepository {

    private static JDBCUserRepository userRepository;

    private final DBConnection dbConnection;
    private final EventBus<Event> eventBus;

    private JDBCUserRepository(DBConnection dbConnection, EventBus<Event> eventBus) {
        this.dbConnection = dbConnection; // Or we can use a UserService that calls an API
        this.eventBus = eventBus;
    }

    public static JDBCUserRepository getInstance(DBConnection dbConnection) {
        if (userRepository == null) {
            Map<Class<RegisterUserEvent>, List<RegisterUserEventSubscription>> subscriptionMap =
                    Collections.singletonMap(
                            RegisterUserEvent.class,
                            Collections.singletonList(
                                    new RegisterUserEventSubscription(new UserPaymentProcessor())
                            )
                    );
            EventBus eventBus = new DefaultEventBus(subscriptionMap);
            userRepository = new JDBCUserRepository(dbConnection, eventBus);
        }
        return userRepository;
    }

    public static JDBCUserRepository getInstance() {
        if (userRepository != null) {
            return userRepository;
        }
        throw new NullPointerException("User Repository has not been set with a database connection & event bus yet.");
    }

    @Override
    public List<User> findAll() {
        return List.of();
    }

    @Override
    public List<User> findAvailableTradesmen() {
        return null;
    }

    @Override
    public Optional<User> findById(UserId id) {
        return Optional.empty();
    }

    @Override
    public boolean doesUserAlreadyExist(User user) throws UserAlreadyExistException {
        return false;
    }

    public User applyForMembership(User user) throws UserMembershipGenericException {
        try {
            System.out.println("============================================");
            System.out.println("New membership application from : " + user);
            System.out.println("============================================\n");

            eventBus.send(RegisterUserEvent.withUser(user));

            return register(user);
        } catch (UserRegistrationException | InvalidUserGenericException e) {
            throw UserMembershipGenericException.membershipApplicationFailed("User data are not valid, we cannot accept his application.", e);
        } catch (Exception e) {
            throw UserMembershipGenericException.membershipApplicationFailed("User payment method is invalid, we cannot accept his application.", e);
        }
    }

    public User register(User user) throws UserAlreadyExistException {
        try {
            doesUserAlreadyExist(user);
            int updated = this.dbConnection.executeUpdate("INSERT INTO users VALUES(...)");
            if (updated == 1) {
                System.out.println("============================================");
                System.out.printf("%s has been registered\n", user);
                System.out.println("============================================\n");
                return user;
            }
            throw new UserRegistrationException("User was not registered. Update response not equal to 1.");
        } catch (SQLException e) {
            throw new UserRegistrationException("User couldn't be registered because of a database connection error.", e);
        }
    }

    @Override
    public UserId nextId() {
        return UserRepository.super.nextId();
    }
}
