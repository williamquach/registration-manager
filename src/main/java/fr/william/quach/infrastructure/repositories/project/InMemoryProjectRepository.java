package fr.william.quach.infrastructure.repositories.project;

import fr.william.quach.application.project.SearchingTradesmanEvent;
import fr.william.quach.application.project.SearchingTradesmanEventSubscription;
import fr.william.quach.domain.enums.ProjectStatus;
import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.project.ProjectId;
import fr.william.quach.domain.repositories.ProjectRepository;
import fr.william.quach.domain.repositories.UserRepository;
import fr.william.quach.infrastructure.errors.project.ProjectAlreadyExistException;
import fr.william.quach.kernel.event.DefaultEventBus;
import fr.william.quach.kernel.event.Event;
import fr.william.quach.kernel.event.EventBus;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class InMemoryProjectRepository implements ProjectRepository {

    private static InMemoryProjectRepository projectRepository;

    private final AtomicInteger counter = new AtomicInteger(0);
    private final Map<ProjectId, Project> data = new ConcurrentHashMap<>();

    private final EventBus<Event> eventBus;

    private InMemoryProjectRepository(EventBus<Event> eventBus) {
        this.eventBus = eventBus;
    }

    public static InMemoryProjectRepository getInstance(UserRepository userRepository) {
        if (projectRepository == null) {
            Map<Class<SearchingTradesmanEvent>, List<SearchingTradesmanEventSubscription>> subscriptionMap =
                    Collections.singletonMap(
                            SearchingTradesmanEvent.class,
                            Collections.singletonList(
                                    new SearchingTradesmanEventSubscription(userRepository, null)
                            )
                    );
            EventBus eventBus = new DefaultEventBus(subscriptionMap);
            projectRepository = new InMemoryProjectRepository(eventBus);
            subscriptionMap.get(SearchingTradesmanEvent.class).get(0).setProjectRepository(projectRepository);
        }
        return projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return new ArrayList<>(data.values());
    }

    @Override
    public Optional<Project> findById(ProjectId id) {
        return Optional.ofNullable(data.get(id));
    }

    @Override
    public boolean doesProjectAlreadyExist(Project project) {
        Optional<Project> projectAlreadyExists = data.values().stream().filter((currentProject) -> Objects.equals(currentProject.getId(), project.getId())).findAny();
        if (projectAlreadyExists.isPresent()) {
            throw new ProjectAlreadyExistException("Project with id : '" + project.getId() + "' already exist");
        }
        return false;
    }

    @Override
    public void createProject(Project project) {
        System.out.println("============================================");
        System.out.println("New project : " + project);
        System.out.println("============================================\n");
        doesProjectAlreadyExist(project);
        register(project);
    }

    @Override
    public Project updateProjectStatus(Project project) {
        if (isNowSearchingTradesman(project)) {
            eventBus.send(SearchingTradesmanEvent.withProject(project));
        }
        register(project);
        return project;
    }

    private boolean isNowSearchingTradesman(Project project) {
        return project.getStatus() == ProjectStatus.SEARCHING_TRADESMAN;
    }

    @Override
    public void register(Project project) {
        data.put(project.getId(), project);
    }

    @Override
    public ProjectId nextId() {
        return ProjectId.of(String.valueOf(counter.incrementAndGet()));
    }
}
