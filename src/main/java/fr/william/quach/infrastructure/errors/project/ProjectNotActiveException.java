package fr.william.quach.infrastructure.errors.project;

import fr.william.quach.domain.models.project.Project;

public class ProjectNotActiveException extends ProjectException {
    public ProjectNotActiveException(Project project) {
        super("Project " + project.getName() + " is not active");
    }
    public ProjectNotActiveException(String message, Exception e) {
        super(message, e);
    }
    public ProjectNotActiveException(String message) {
        super(message);
    }
}
