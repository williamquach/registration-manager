package fr.william.quach.infrastructure.errors.users;

public class InvalidUserGenericException extends RuntimeException {

    public InvalidUserGenericException(String message, Exception e) {
        super(message, e);
    }

    public InvalidUserGenericException(String message) {
        super(message);
    }

    /** In valid Email Address */
    public static InvalidUserGenericException invalidEmailAddressFormat() {
        return new InvalidUserGenericException("Email format is invalid");
    }

    public static InvalidUserGenericException invalidEmailAddressFormat(String message) {
        return new InvalidUserGenericException(message);
    }

    public static InvalidUserGenericException invalidEmailAddressFormat(String message, Exception e) {
        return new InvalidUserGenericException(message, e);
    }

    /** Invalid password */
    public static InvalidUserGenericException invalidPasswordFormat() {
        return new InvalidUserGenericException("User password cannot be null and must contain at least 8 characters");
    }

    public static InvalidUserGenericException invalidPasswordFormat(String message) {
        return new InvalidUserGenericException(message);
    }

    public static InvalidUserGenericException invalidPasswordFormat(String message, Exception e) {
        return new InvalidUserGenericException(message, e);
    }

    /** Invalid fields */
    public static InvalidUserGenericException invalidFields() {
        return new InvalidUserGenericException("One of user field is incorrect.");
    }

    public static InvalidUserGenericException invalidFields(String message) {
        return new InvalidUserGenericException(message);
    }

    public static InvalidUserGenericException invalidFields(String message, Exception e) {
        return new InvalidUserGenericException(message, e);
    }
}
