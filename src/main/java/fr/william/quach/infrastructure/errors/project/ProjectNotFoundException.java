package fr.william.quach.infrastructure.errors.project;

import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.user.User;

public class ProjectNotFoundException extends ProjectException {
    public ProjectNotFoundException(String projectId) {
        super("Project with id " + projectId + " not found");
    }
    public ProjectNotFoundException(String message, Exception e) {
        super(message, e);
    }
}
