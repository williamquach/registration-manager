package fr.william.quach.infrastructure.errors.project;

public class ProjectUpdateException extends ProjectException {
    public ProjectUpdateException(String projectId) {
        super("Project with id " + projectId + " couldn't be updated");
    }
    public ProjectUpdateException(String message, Exception e) {
        super(message, e);
    }
}
