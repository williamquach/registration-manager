package fr.william.quach.infrastructure.errors.spring;

import fr.william.quach.infrastructure.errors.users.InvalidUserGenericException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class UserInvalidAdvice {
    @ResponseBody
    @ExceptionHandler(InvalidUserGenericException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String userInvalid(InvalidUserGenericException ex) {
        return ex.getMessage();
    }
}
