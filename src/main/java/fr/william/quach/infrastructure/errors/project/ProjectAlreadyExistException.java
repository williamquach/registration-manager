package fr.william.quach.infrastructure.errors.project;

public class ProjectAlreadyExistException extends ProjectException {
    public ProjectAlreadyExistException(String message, Exception e) {
        super(message, e);
    }
    public ProjectAlreadyExistException(String message) {
        super(message);
    }
}
