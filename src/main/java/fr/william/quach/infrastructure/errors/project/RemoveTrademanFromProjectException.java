package fr.william.quach.infrastructure.errors.project;

import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.user.User;

public class RemoveTrademanFromProjectException extends ProjectException {
    public RemoveTrademanFromProjectException(User user, Project project) {
        super("User " + user.getFullName() + " is not part of project :" + project.getName());
    }
    public RemoveTrademanFromProjectException(String message, Exception e) {
        super(message, e);
    }
    public RemoveTrademanFromProjectException(String message) {
        super(message);
    }
}
