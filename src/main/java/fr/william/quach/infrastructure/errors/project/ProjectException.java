package fr.william.quach.infrastructure.errors.project;

public class ProjectException extends RuntimeException {
    public ProjectException(String message, Exception e) {
        super(message, e);
    }
    public ProjectException(String message) {
        super(message);
    }
}
