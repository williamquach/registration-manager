package fr.william.quach.infrastructure.errors.project;

import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.user.User;

public class TradesmanDuplicateException extends ProjectException {
    public TradesmanDuplicateException(User tradesman, Project project) {
        super("Tradesman " + tradesman.getFullName() + " is already in project: " + project.getName());
    }
    public TradesmanDuplicateException(String message, Exception e) {
        super(message, e);
    }
    public TradesmanDuplicateException(String message) {
        super(message);
    }
}
