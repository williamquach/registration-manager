package fr.william.quach.infrastructure.errors.payment;

public class PaymentFailedGenericException extends RuntimeException {

    public PaymentFailedGenericException(String message, Exception e) {
        super(message, e);
    }

    public PaymentFailedGenericException(String message) {
        super(message);
    }

    /** AMOUNT */
    public static PaymentFailedGenericException invalidAmount() {
        return new PaymentFailedGenericException("Payment amount is invalid");
    }

    public static PaymentFailedGenericException invalidAmount(String message) {
        return new PaymentFailedGenericException(message);
    }

    public static PaymentFailedGenericException invalidAmount(String message, Exception e) {
        return new PaymentFailedGenericException(message, e);
    }

    /** Credit Card */
    public static PaymentFailedGenericException invalidCreditCard(String message, Exception e) {
        return new PaymentFailedGenericException(message, e);
    }

    public static PaymentFailedGenericException invalidCreditCard(String message) {
        return new PaymentFailedGenericException(message);
    }

}
