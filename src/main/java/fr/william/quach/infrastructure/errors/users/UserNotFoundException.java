package fr.william.quach.infrastructure.errors.users;

import fr.william.quach.domain.models.user.UserId;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(UserId id) {
        super("Could not find user with id : " + id.getValue());
    }
    public UserNotFoundException(String message, Exception e) {
        super(message, e);
    }
    public UserNotFoundException(String message) {
        super(message);
    }
}
