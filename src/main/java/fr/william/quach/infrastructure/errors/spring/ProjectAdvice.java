package fr.william.quach.infrastructure.errors.spring;

import fr.william.quach.infrastructure.errors.project.ProjectException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ProjectAdvice {
    @ResponseBody
    @ExceptionHandler(ProjectException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String projectNotFound(ProjectException ex) {
        return ex.getMessage();
    }
}
