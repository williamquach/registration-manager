package fr.william.quach.infrastructure.errors.users;

public class UserException extends RuntimeException {
    public UserException(String message, Exception e) {
        super(message, e);
    }

    public UserException(String message) {
        super(message);
    }
}
