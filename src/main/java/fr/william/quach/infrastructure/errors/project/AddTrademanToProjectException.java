package fr.william.quach.infrastructure.errors.project;

import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.user.User;

public class AddTrademanToProjectException extends ProjectException {
    public AddTrademanToProjectException(User user, Project project) {
        super("User " + user.getFullName() + " is not a tradesman. Project name :" + project.getName());
    }
    public AddTrademanToProjectException(String message, Exception e) {
        super(message, e);
    }
    public AddTrademanToProjectException(String message) {
        super(message);
    }
}
