package fr.william.quach.infrastructure.errors.users;

public class UserMembershipGenericException extends RuntimeException {

    public UserMembershipGenericException(String message, Exception e) {
        super(message, e);
    }

    public UserMembershipGenericException(String message) {
        super(message);
    }

    /** Membership application */
    public static UserMembershipGenericException membershipApplicationFailed() {
        return new UserMembershipGenericException("User membership application failed.");
    }

    public static UserMembershipGenericException membershipApplicationFailed(String message) {
        return new UserMembershipGenericException(message);
    }

    public static UserMembershipGenericException membershipApplicationFailed(String message, Exception e) {
        return new UserMembershipGenericException(message, e);
    }

    /** Prepayment */
    public static UserMembershipGenericException prepaymentFailed() {
        return new UserMembershipGenericException("User prepayment failed.");
    }

    public static UserMembershipGenericException prepaymentFailed(String message) {
        return new UserMembershipGenericException(message);
    }

    public static UserMembershipGenericException prepaymentFailed(String message, Exception e) {
        return new UserMembershipGenericException(message, e);
    }
}
