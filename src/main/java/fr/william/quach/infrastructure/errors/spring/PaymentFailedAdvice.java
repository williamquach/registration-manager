package fr.william.quach.infrastructure.errors.spring;

import fr.william.quach.infrastructure.errors.payment.PaymentFailedGenericException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class PaymentFailedAdvice {
    @ResponseBody
    @ExceptionHandler(PaymentFailedGenericException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String paymentFailed(PaymentFailedGenericException ex) {
        return ex.getMessage();
    }
}
