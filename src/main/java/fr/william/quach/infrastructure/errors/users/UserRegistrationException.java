package fr.william.quach.infrastructure.errors.users;

public class UserRegistrationException extends RuntimeException {
    public UserRegistrationException(String message, Exception e) {
        super(message, e);
    }
    public UserRegistrationException(String message) {
        super(message);
    }
}
