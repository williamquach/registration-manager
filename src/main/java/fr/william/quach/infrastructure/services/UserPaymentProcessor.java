package fr.william.quach.infrastructure.services;

import fr.william.quach.domain.models.payment.Amount;
import fr.william.quach.domain.models.payment.CreditCard.CreditCard;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.domain.models.user.UserId;
import fr.william.quach.infrastructure.errors.payment.PaymentFailedGenericException;
import fr.william.quach.infrastructure.errors.users.InvalidUserGenericException;
import fr.william.quach.infrastructure.errors.users.UserMembershipGenericException;

public class UserPaymentProcessor {

    public static User ourCompanyBankAccount;

    static {
        try {
            UserId userId = UserId.of("admin");
            ourCompanyBankAccount = User.create(
                    userId,
                    "Patreon",
                    "Lebosse",
                    "pleboss@e-commerce.com",
                    "theBestPasswordEver",
                    CreditCard.create("1234678946570000", 1, 2030, "000"),
                    null
            );
        } catch (InvalidUserGenericException e) {
            e.printStackTrace();
        }
    }

    public boolean processPayment(Amount amount, User user) throws UserMembershipGenericException {
        System.out.println("DEBUG processPayment");
        System.out.println("DEBUG processPayment");
        System.out.println("DEBUG processPayment");
        if (!user.getPaymentMethod().isValidByPrepayment()) { // Payment succeed
            throw UserMembershipGenericException.prepaymentFailed(
                    String.format(
                            "User with email %s cannot subscribe membership because of prepayment process failed",
                            user.getEmailAddress()
                    )
            );
        }
        System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
        System.out.println("Processing payment... from " + user);
        boolean success = user.getPaymentMethod().pay(amount, ourCompanyBankAccount);
        if (success) {
            System.out.println("Processed payment succeed from " + user);
            System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n");
        } else {
            System.out.println("Processed payment failed from " + user);
            System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n");
            throw new PaymentFailedGenericException("Payment failed from user : " + user);
        }
        return true;
    }
}
