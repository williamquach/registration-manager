package fr.william.quach.infrastructure.services.Scheduler;

import fr.william.quach.kernel.Scheduler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SchedulerService implements Scheduler {

    public SchedulerService() {}

    public boolean launchScheduler(Runnable task, long delay, long period, TimeUnit unit) throws InterruptedException {
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(task, delay, period, unit);
        return executorService.awaitTermination(2, TimeUnit.SECONDS);
    }

}
