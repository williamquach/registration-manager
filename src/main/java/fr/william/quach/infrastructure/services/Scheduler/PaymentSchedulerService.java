package fr.william.quach.infrastructure.services.Scheduler;

import fr.william.quach.domain.models.payment.Amount;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.infrastructure.services.UserPaymentProcessor;

import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

public class PaymentSchedulerService {

    private final SchedulerService schedulerService;

    public PaymentSchedulerService(SchedulerService schedulerService) {
        this.schedulerService = schedulerService;
    }

    public void launchMonthlyPayment(User user, Amount amount) throws InterruptedException {
        LocalDate firstPaymentDate = LocalDate.now();
        Runnable task = monthlyPayment(user, amount, firstPaymentDate);
        schedulerService.launchScheduler(task, 27, 1, TimeUnit.DAYS);
    }

    private Runnable monthlyPayment(User user, Amount amount, LocalDate firstPaymentDate) {
        return () -> {
            if (shouldUserPayToday(firstPaymentDate)) {
                payMonthly(user, amount);
            }
        };
    }

    public boolean shouldUserPayToday(LocalDate firstPaymentDate) {
        int firstPaymentDay = firstPaymentDate.getDayOfMonth();
        int firstPaymentMonth = firstPaymentDate.getMonthValue();
        int firstPaymentYear = firstPaymentDate.getYear();

        LocalDate currentScheduledTaskDate = LocalDate.now();
        int currentScheduledTaskDay = firstPaymentDate.getDayOfMonth();
        int currentScheduledTaskMonth = firstPaymentDate.getMonthValue();
        int currentScheduledTaskYear = firstPaymentDate.getYear();

        if (firstPaymentDate.isBefore(currentScheduledTaskDate))
            if (firstPaymentYear <= currentScheduledTaskYear)
                if (firstPaymentMonth < currentScheduledTaskMonth)
                    return firstPaymentDay == currentScheduledTaskDay || firstPaymentDay >= 28 && currentScheduledTaskDay >= 28;
        return false;
    }

    public void payMonthly(User user, Amount amount) {
        UserPaymentProcessor userPaymentProcessor = new UserPaymentProcessor();
        userPaymentProcessor.processPayment(amount, user);
    }
}
