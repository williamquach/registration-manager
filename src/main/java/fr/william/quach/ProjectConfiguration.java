package fr.william.quach;

import fr.william.quach.application.project.*;
import fr.william.quach.domain.repositories.ProjectRepository;
import fr.william.quach.infrastructure.repositories.project.InMemoryProjectRepository;
import fr.william.quach.kernel.command.Command;
import fr.william.quach.kernel.command.CommandBus;
import fr.william.quach.kernel.command.CommandHandler;
import fr.william.quach.kernel.command.SimpleCommandBus;
import fr.william.quach.kernel.query.Query;
import fr.william.quach.kernel.query.QueryBus;
import fr.william.quach.kernel.query.QueryHandler;
import fr.william.quach.kernel.query.SimpleQueryBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.Map;

@Configuration
public class ProjectConfiguration {
    @Bean
    public ProjectRepository projectRepository() {
        return InMemoryProjectRepository.getInstance(UserConfiguration.userRepository());
    }

    @Bean
    public CreateProjectCommandHandler createProjectCommandHandler() {
        return new CreateProjectCommandHandler(UserConfiguration.userRepository(), projectRepository());
    }

    @Bean
    public UpdateProjectStatusCommandHandler updateProjectStatusCommandHandler() {
        return new UpdateProjectStatusCommandHandler(projectRepository());
    }

    @Bean
    public QueryBus projectQueryBus() {
        final Map<Class<? extends Query>, QueryHandler> queryHandlerMap =
                Collections.singletonMap(GetProjectBestTradesmanMatch.class, new GetProjectBestTradesmanMatchQueryHandler(projectRepository()));
        return new SimpleQueryBus(queryHandlerMap);
    }

    @Bean
    public CommandBus projectCommandBus() {
        final Map<Class<? extends Command>, CommandHandler> commandHandlerMap =
                Map.of(
                        CreateProject.class, new CreateProjectCommandHandler(UserConfiguration.userRepository(), projectRepository()),
                        UpdateProjectStatus.class, new UpdateProjectStatusCommandHandler(projectRepository()),
                        AssignTradesmanToProject.class, new AssignTradesmanToProjectCommandHandler(UserConfiguration.userRepository(), projectRepository())
                );
        return new SimpleCommandBus(commandHandlerMap);
    }
}
