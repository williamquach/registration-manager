package fr.william.quach.kernel.event;

import java.time.LocalDateTime;

public interface Event {

    EventId getId();

    LocalDateTime getOccurredDate();
}
