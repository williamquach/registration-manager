package fr.william.quach.kernel.event;

import fr.william.quach.kernel.Subscriber;

import java.util.List;

public interface EventBus<E extends Event> {
    void send(E event);

    void registerSubscriber(Class<E> classE, List<Subscriber<E>> subscribers);
}
