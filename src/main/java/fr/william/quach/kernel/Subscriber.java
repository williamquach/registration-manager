package fr.william.quach.kernel;

import fr.william.quach.kernel.event.Event;

import java.util.function.Consumer;

public interface Subscriber<E extends Event> extends Consumer<E> {}
