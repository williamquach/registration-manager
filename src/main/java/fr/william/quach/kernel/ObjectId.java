package fr.william.quach.kernel;

import fr.william.quach.domain.models.project.ProjectId;

import java.util.Objects;
import java.util.UUID;

public abstract class ObjectId {

    private final String value;

    public ObjectId(String value) {
        this.value = value;
    }

    public static ObjectId fromUUID(UUID uuid) {
        return new ProjectId(uuid.toString());
    }

    public static ObjectId of(String value) {
        return new ProjectId(value);
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectId id = (ObjectId) o;
        return Objects.equals(value, id.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "ObjectId{" +
                "value=" + value +
                '}';
    }
}
