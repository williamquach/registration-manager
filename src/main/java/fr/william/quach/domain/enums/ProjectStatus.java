package fr.william.quach.domain.enums;

public enum ProjectStatus {
    NO_TRADESMAN("No tradesman"),
    SEARCHING_TRADESMAN("Searching for tradesman"),
    FOUND_TRADESMAN("Found tradesman"),
    TRADESMAN_ASSIGNED("Tradesman assigned to project");

    public final String description;

    ProjectStatus(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
