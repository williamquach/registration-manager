package fr.william.quach.domain.models.project;

import fr.william.quach.kernel.ObjectId;

import java.util.UUID;

final public class ProjectId extends ObjectId {
    public ProjectId(String value) {
        super(value);
    }

    public static ProjectId fromUUID(UUID uuid) {
        return new ProjectId(uuid.toString());
    }

    public static ProjectId of(String value) {
        return new ProjectId(value);
    }
}
