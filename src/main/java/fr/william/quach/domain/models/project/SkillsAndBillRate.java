package fr.william.quach.domain.models.project;

import java.util.List;

public class SkillsAndBillRate {
    private List<Skill> skills;
    private BillRate billRate;

    public SkillsAndBillRate(List<Skill> skills, BillRate billRate) {
        this.skills = skills;
        this.billRate = billRate;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public BillRate getBillRate() {
        return billRate;
    }

    public void addSkill(Skill skill) {
        if (!getSkills().contains(skill)) {
            skills.add(skill);
        }
    }

    public void setBillRate(BillRate billRate) {
        if (getBillRate() == null) {
            this.billRate = billRate;
        }
    }

    @Override
    public String toString() {
        return "SkillsAndBillRate{" +
                "skills=" + skills +
                ", billRate=" + billRate +
                '}';
    }
}
