package fr.william.quach.domain.models.project;

public class BillRate {
    private float amountPerDay;

    public BillRate(float amountPerDay) {
        this.amountPerDay = amountPerDay;
    }

    public float getAmountPerDay() {
        return amountPerDay;
    }
}
