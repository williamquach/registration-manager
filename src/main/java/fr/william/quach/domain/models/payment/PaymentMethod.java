package fr.william.quach.domain.models.payment;

import fr.william.quach.domain.models.payment.CreditCard.CreditCard;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.exposition.user.CreditCardRequest;
import fr.william.quach.exposition.user.PaymentMethodRequest;

import java.util.Calendar;

public interface PaymentMethod {
    boolean isValidByPrepayment();

    boolean pay(Amount amount, User receiver);

    static PaymentMethod getPaymentMethodModelFromRequest(PaymentMethodRequest paymentMethodRequest) {
        PaymentMethod paymentMethod = null;

        if (paymentMethodRequest instanceof CreditCardRequest) {
            paymentMethod = CreditCard.create(
                    ((CreditCardRequest) paymentMethodRequest).creditCardNumbers,
                    ((CreditCardRequest) paymentMethodRequest).expirationMonth,
                    ((CreditCardRequest) paymentMethodRequest).expirationYear,
                    ((CreditCardRequest) paymentMethodRequest).cryptogram
            );
        }

        return paymentMethod;
    }

    static PaymentMethodRequest getPaymentMethodRequestFromModel(PaymentMethod paymentMethod) {
        PaymentMethodRequest paymentMethodRequest = null;

        if (paymentMethod instanceof CreditCard) {
            paymentMethodRequest = new CreditCardRequest(
                    ((CreditCard) paymentMethod).getCreditCardNumbers().getSixteenNumbers(),
                    ((CreditCard) paymentMethod).getExpirationDate().getDate().get(Calendar.MONTH),
                    ((CreditCard) paymentMethod).getExpirationDate().getDate().get(Calendar.YEAR),
                    ((CreditCard) paymentMethod).getCryptogram()
            );
        }

        return paymentMethodRequest;
    }
}
