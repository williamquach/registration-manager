package fr.william.quach.domain.models.payment;

import fr.william.quach.infrastructure.errors.payment.PaymentFailedGenericException;

import java.util.Objects;

public class Amount {
    private final double value;
    private final Currency currency;

    public Amount(double value, Currency unit) throws PaymentFailedGenericException {
        validate(value);
        this.value = value;
        this.currency = unit;
    }

    private void validate(double value) throws PaymentFailedGenericException {
        if (value < 0) {
            throw PaymentFailedGenericException.invalidAmount("Payment amount cannot be negative");
        }
    }

    public double getValue() {
        return value;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Amount amount = (Amount) o;
        return Double.compare(amount.value, value) == 0 && Objects.equals(currency, amount.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, currency);
    }

    @Override
    public String toString() {
        return value + "" + currency.getName();
    }
}
