package fr.william.quach.domain.models.user;

import fr.william.quach.domain.models.project.Project;

public class Contractor implements UserRole {
    public void requestTradesman(Project project) {
        System.out.println("request tradesman in Contractor");
    }
}
