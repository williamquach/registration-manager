package fr.william.quach.domain.models.user;

import fr.william.quach.kernel.ObjectId;

import java.util.UUID;

final public class UserId extends ObjectId {
    public UserId(String value) {
        super(value);
    }

    public static UserId fromUUID(UUID uuid) {
        return new UserId(uuid.toString());
    }

    public static UserId of(String value) {
        return new UserId(value);
    }
}
