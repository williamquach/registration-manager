package fr.william.quach.domain.models.user;

import fr.william.quach.infrastructure.errors.users.InvalidUserGenericException;

public class Password {
    private final String password;

    public Password(String password) {
        this.password = password;
    }

    public static Password create(String password) throws InvalidUserGenericException {
        if (!Password.isValid(password)) {
            throw InvalidUserGenericException.invalidPasswordFormat();
        }
        return new Password(Password.getHashedPassword(password));
    }

    public static boolean isValid(String password) {
        return password != null && password.length() >= 8;
    }

    private static String getHashedPassword(String password) {
        return password;
    }

    public boolean isSame(String password) {
        return Password.getHashedPassword(password).equals(this.password);
    }

    public String getPassword() {
        return password;
    }
}
