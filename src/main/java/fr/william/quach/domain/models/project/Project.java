package fr.william.quach.domain.models.project;

import fr.william.quach.domain.enums.ProjectStatus;
import fr.william.quach.domain.models.user.Contractor;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.exposition.project.ProjectResponse;
import fr.william.quach.exposition.project.SkillsAndBillRateByJobResponse;
import fr.william.quach.infrastructure.errors.project.AddTrademanToProjectException;
import fr.william.quach.infrastructure.errors.project.TradesmanDuplicateException;

import java.util.ArrayList;
import java.util.List;

public class Project {
    private ProjectId id;
    private String name;
    private User leader;
    private List<User> tradesmen;
    private List<Task> tasks;
    private SkillsAndBillRateByJob skillsAndBillRateByJob;
    private Location location;
    private Duration duration;
    private boolean isActive;
    private ProjectStatus status;

    private User bestTradesmanMatch;

    public Project(ProjectId id, String name, User leader, ProjectStatus status) {
        this.id = id;
        this.name = name;
        this.leader = leader;
        this.isActive = true;
        this.status = status;
        this.tradesmen = new ArrayList<>();
        this.tasks = new ArrayList<>();
    }

    public Project(String name) {
        this.name = name;
    }

    public ProjectId getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public User getLeader() {
        return leader;
    }

    public User getBestTradesmanMatch() {
        return bestTradesmanMatch;
    }

    public boolean isActive() {
        return isActive;
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public List<User> getTradesmen() {
        return tradesmen;
    }

    public SkillsAndBillRateByJob getSkillsAndBillRateByJob() {
        return skillsAndBillRateByJob;
    }

    public Duration getDuration() {
        return duration;
    }

    public Location getLocation() {
        return location;
    }

    public User addTradesman(User tradesman) {
        if (tradesman.getRole() instanceof Contractor) {
            throw new AddTrademanToProjectException(tradesman, this);
        }
        if (tradesmen.contains(tradesman)) {
            throw new TradesmanDuplicateException(tradesman, this);
        }
        this.tradesmen.add(tradesman);
        return tradesman;
    }

    public void removeTradesman(User tradesman) {
        if (!tradesmen.contains(tradesman)) {
            throw new TradesmanDuplicateException(tradesman, this);
        }
        this.tradesmen.remove(tradesman);
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public void setSkillsAndBillRateByJob(SkillsAndBillRateByJob skillsAndBillRateByJob) {
        this.skillsAndBillRateByJob = skillsAndBillRateByJob;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public void updateStatus(ProjectStatus status) {
        this.status = status;
    }

    public void setBestTradesmanMatch(User bestTradesmanMatch) {
        this.bestTradesmanMatch = bestTradesmanMatch;
    }

    public User matchNeeds(List<User> tradesmen) {
        return tradesmen.get(0);
    }

    public ProjectResponse toResponse() {
        List<SkillsAndBillRateByJobResponse> skillsAndBillRateByJobResponses =
                getSkillsAndBillRateByJob().toSkillsAndBillRateByJobResponses();

        return new ProjectResponse(
                getId(),
                getName(),
                getLeader(),
                getTradesmen(),
                getTasks(),
                skillsAndBillRateByJobResponses,
                getLocation(),
                getDuration(),
                isActive(),
                getStatus()
        );
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isActive=" + isActive +
                ", status=" + status +
                ", location=" + location +
                ", duration=" + duration +
                '}';
    }
}
