package fr.william.quach.domain.models.user;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonSubTypes({
        @JsonSubTypes.Type(value = Contractor.class, name = "contractor"),
        @JsonSubTypes.Type(value = Tradesman.class, name = "tradesman"),
})
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.WRAPPER_OBJECT,
        property = "type"
)
public interface UserRole {
}
