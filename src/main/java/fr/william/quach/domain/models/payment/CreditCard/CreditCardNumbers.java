package fr.william.quach.domain.models.payment.CreditCard;

import fr.william.quach.infrastructure.errors.payment.PaymentFailedGenericException;

public class CreditCardNumbers {
    private final String sixteenNumbers;

    private CreditCardNumbers(String sixteenNumbers) {
        this.sixteenNumbers = sixteenNumbers;
    }

    public static CreditCardNumbers create(String sixteenNumbers) {
        validate(sixteenNumbers);
        return new CreditCardNumbers(sixteenNumbers);
    }

    public String getSixteenNumbers() {
        return sixteenNumbers;
    }

    static void validate(String sixteenNumbers) {
        if (sixteenNumbers == null || sixteenNumbers.length() != 16) {
            throw PaymentFailedGenericException.invalidCreditCard("Credit card numbers must have 16 numbers");
        }
    }
}
