package fr.william.quach.domain.models.user;

public class Tradesman implements UserRole {

    private boolean isAvailable;

    public Tradesman() {
        this.isAvailable = true;
    }

    public void makeOccupied() {
        this.isAvailable = false;
    }

    public void makeAvailable() {
        this.isAvailable = true;
    }

    public boolean isAvailable() {
        return isAvailable;
    }
}
