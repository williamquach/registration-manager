package fr.william.quach.domain.models.project;

import fr.william.quach.exposition.project.BillRateResponse;
import fr.william.quach.exposition.project.JobResponse;
import fr.william.quach.exposition.project.SkillResponse;
import fr.william.quach.exposition.project.SkillsAndBillRateByJobResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SkillsAndBillRateByJob {
    private Map<Job, SkillsAndBillRate> skillsAndBillRateByJob;

    public SkillsAndBillRateByJob(Map<Job, SkillsAndBillRate> skillsAndBillRateByJob) {
        this.skillsAndBillRateByJob = skillsAndBillRateByJob;
    }

    public void addSkillToJob(Skill skill, Job job) {
        SkillsAndBillRate jobSkillsAndBillRate = this.skillsAndBillRateByJob.get(job);
        jobSkillsAndBillRate.addSkill(skill);
    }

    public void setBillRateToJob(BillRate billRate, Job job) {
        SkillsAndBillRate jobSkillsAndBillRate = this.skillsAndBillRateByJob.get(job);
        jobSkillsAndBillRate.setBillRate(billRate);
    }

    public Map<Job, SkillsAndBillRate> getSkillsAndBillRateByJob() {
        return skillsAndBillRateByJob;
    }

    public List<SkillsAndBillRateByJobResponse> toSkillsAndBillRateByJobResponses() {
        List<SkillsAndBillRateByJobResponse> skillsAndBillRateByJobResponses = new ArrayList<>();

        SkillsAndBillRateByJobResponse skillsAndBillRateByJobResponse;
        for (Map.Entry<Job, SkillsAndBillRate> jobSkillsAndBillRateEntry : skillsAndBillRateByJob.entrySet()) {
            List<SkillResponse> skillsResponse = jobSkillsAndBillRateEntry.getValue().getSkills().stream().map(skill ->
                    new SkillResponse(skill.getName(), skill.getDescription())
            ).collect(Collectors.toList());

            skillsAndBillRateByJobResponse = new SkillsAndBillRateByJobResponse(
                    new JobResponse(jobSkillsAndBillRateEntry.getKey().getName(), jobSkillsAndBillRateEntry.getKey().getDescription()),
                    skillsResponse,
                    new BillRateResponse(jobSkillsAndBillRateEntry.getValue().getBillRate().getAmountPerDay())
            );
            skillsAndBillRateByJobResponses.add(skillsAndBillRateByJobResponse);
        }
        return skillsAndBillRateByJobResponses;
    }

    @Override
    public String toString() {
        return "SkillsAndBillRateByJob{" +
                "skillsAndBillRateByJob=" + skillsAndBillRateByJob +
                '}';
    }
}
