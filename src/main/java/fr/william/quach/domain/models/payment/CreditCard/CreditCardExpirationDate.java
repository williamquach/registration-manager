package fr.william.quach.domain.models.payment.CreditCard;

import fr.william.quach.infrastructure.errors.payment.PaymentFailedGenericException;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;

public class CreditCardExpirationDate {
    private final Calendar expirationDate;

    public Calendar getDate() {
        return expirationDate;
    }

    private CreditCardExpirationDate(Calendar expirationDate) {
        this.expirationDate = expirationDate;
    }

    public static CreditCardExpirationDate create(int expirationMonth, int expirationYear) {
        LocalDate initial = LocalDate.of(expirationYear, expirationMonth, 1);
        LocalDate tempExpirationDate = initial.withDayOfMonth(initial.lengthOfMonth());
        Date expirationDate = Date.from(tempExpirationDate.atStartOfDay().toInstant(ZoneOffset.UTC));

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(expirationDate);
        validate(calendar);
        return new CreditCardExpirationDate(calendar);
    }

    static void validate(Calendar expirationDate) {
        if (expirationDateIsInvalid(expirationDate)) {
            throw PaymentFailedGenericException.invalidCreditCard("Credit card is expired");
        }
    }

    private static boolean expirationDateIsInvalid(Calendar expirationDate) {
        Calendar now = Calendar.getInstance();
        return  expirationDate.get(Calendar.YEAR) < now.get(Calendar.YEAR) ||
                expirationDate.get(Calendar.YEAR) == now.get(Calendar.YEAR) && expirationDate.get(Calendar.MONTH) < now.get(Calendar.MONTH);
    }
}
