package fr.william.quach.domain.models.user;

import fr.william.quach.domain.models.payment.PaymentMethod;
import fr.william.quach.infrastructure.errors.users.InvalidUserGenericException;

import java.util.Objects;

public class User {
    private final UserId userId;
    private final String firstname;
    private final String lastname;
    private final Email email;
    private final Password password;
    private final PaymentMethod paymentMethod;
    private final UserRole role;

    private User(UserId userId, String firstname, String lastname, Email email, Password password, PaymentMethod paymentMethod, UserRole role) {
        this.userId = userId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.paymentMethod = paymentMethod;
        this.role = role;
    }

    public static User create(UserId userId, String firstname, String lastname, String email, String password, PaymentMethod paymentMethod, UserRole role) throws InvalidUserGenericException {
        User.validate(firstname, lastname, email, password);
        return new User(userId, firstname, lastname, Email.create(email), Password.create(password), paymentMethod, role);
    }

    private static void validate(String firstname, String lastname, String email, String password) throws InvalidUserGenericException {
        if (firstname == null || firstname.equals("")) {
            throw InvalidUserGenericException.invalidFields("User firstname cannot be null nor empty");
        }
        if (lastname == null || lastname.equals("")) {
            throw InvalidUserGenericException.invalidFields("User lastname cannot be null nor empty");
        }
        if (!Email.isValid(email)) {
            throw InvalidUserGenericException.invalidFields("User email is not valid");
        }
        if (!Password.isValid(password)) {
            throw InvalidUserGenericException.invalidPasswordFormat();
        }
    }

    public UserId getUserId() {
        return userId;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmailAddress() {
        return email.getEmailAddress();
    }

    public Password getPassword() {
        return password;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public String getFullName() {
        return String.format("%s %s", this.getFirstname(), this.getLastname());
    }

    public UserRole getRole() {
        return role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userId, user.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }

    @Override
    public String toString() {
        return String.format(
                "User { lastname = '%s' | firstname = '%s' | email = '%s' | role = '%s' }",
                getLastname(),
                getFirstname(),
                getEmailAddress(),
                getRole()
        );
    }
}
