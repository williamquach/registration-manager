package fr.william.quach.domain.models.project;

public class Location {
    private String streetNumber;
    private String streetName;
    private String city;
    private String country;

    public Location(String streetNumber, String streetName, String city, String country) {
        this.streetNumber = streetNumber;
        this.streetName = streetName;
        this.city = city;
        this.country = country;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}
