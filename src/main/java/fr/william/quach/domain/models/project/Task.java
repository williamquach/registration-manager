package fr.william.quach.domain.models.project;


public class Task {
    private String name;
    private String description;
    private Duration duration;

    public Task(String name, String description, Duration duration) {
        this.name = name;
        this.description = description;
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Duration getDuration() {
        return duration;
    }
}
