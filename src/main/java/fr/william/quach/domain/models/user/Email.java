package fr.william.quach.domain.models.user;

import fr.william.quach.infrastructure.errors.users.InvalidUserGenericException;

import java.util.regex.Pattern;

public class Email {
    private final String emailAddress;

    private Email(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public static Email create(String emailAddress) throws InvalidUserGenericException {
        if (!Email.isValid(emailAddress)) {
            throw InvalidUserGenericException.invalidEmailAddressFormat("Email format is invalid");
        }
        return new Email(emailAddress);
    }

    public static boolean isValid(String emailAddress) {
        if(emailAddress == null) {
            return false;
        }
        String regexPattern = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        return Pattern.compile(regexPattern)
                .matcher(emailAddress)
                .matches();
    }

    public String getEmailAddress() {
        return emailAddress;
    }
}
