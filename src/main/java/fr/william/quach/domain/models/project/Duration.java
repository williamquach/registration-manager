package fr.william.quach.domain.models.project;

import java.time.LocalDate;

public class Duration {
    private int numberOfDays;
    private LocalDate startDate;

    public Duration(int numberOfDays, LocalDate startDate) {
        this.numberOfDays = numberOfDays;
        this.startDate = startDate;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public LocalDate getStartDate() {
        return startDate;
    }
}
