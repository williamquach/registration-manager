package fr.william.quach.domain.models.payment;

import java.util.Objects;

public class Currency {
    private final String country; // It will get too far if value object country
    private final String name;
    private final String currencySymbol;
    private final String isoCode; // ISO 4217 -> Euro = EUR


    public Currency(String country, String name, String currencySymbol, String isoCode) {
        this.country = country;
        this.name = name;
        this.currencySymbol = currencySymbol;
        this.isoCode = isoCode;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return Objects.equals(country, currency.country) && Objects.equals(name, currency.name) && Objects.equals(currencySymbol, currency.currencySymbol) && Objects.equals(isoCode, currency.isoCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, name, currencySymbol, isoCode);
    }
}
