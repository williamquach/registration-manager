package fr.william.quach.domain.models.payment.CreditCard;

import fr.william.quach.domain.models.payment.Amount;
import fr.william.quach.domain.models.payment.PaymentMethod;
import fr.william.quach.domain.models.user.User;
import fr.william.quach.infrastructure.errors.payment.PaymentFailedGenericException;

public class CreditCard implements PaymentMethod {

    private final CreditCardNumbers creditCardNumbers;
    private final CreditCardExpirationDate expirationDate;
    private final String cryptogram;

    private CreditCard(CreditCardNumbers sixteenNumbers, CreditCardExpirationDate expirationDate, String cryptogram) {
        this.creditCardNumbers = sixteenNumbers;
        this.expirationDate = expirationDate;
        this.cryptogram = cryptogram;
    }

    public static CreditCard create(String sixteenNumbers, int expirationMonth, int expirationYear, String cryptogram) throws PaymentFailedGenericException {
        CreditCardNumbers creditCardNumbers = CreditCardNumbers.create(sixteenNumbers);
        CreditCardExpirationDate creditCardExpirationDate = CreditCardExpirationDate.create(expirationMonth, expirationYear);
        validate(creditCardNumbers, creditCardExpirationDate, cryptogram);
        return new CreditCard(creditCardNumbers, creditCardExpirationDate, cryptogram);
    }

    private static void validate(CreditCardNumbers creditCardNumbers, CreditCardExpirationDate expirationDate, String cryptogram) throws PaymentFailedGenericException {
        CreditCardNumbers.validate(creditCardNumbers.getSixteenNumbers());
        CreditCardExpirationDate.validate(expirationDate.getDate());
        if (cryptogram == null || cryptogram.length() != 3) {
            throw PaymentFailedGenericException.invalidCreditCard("Credit card cryptogram must have 3 numbers");
        }
    }

    public CreditCardNumbers getCreditCardNumbers() {
        return creditCardNumbers;
    }

    public CreditCardExpirationDate getExpirationDate() {
        return expirationDate;
    }

    public String getCryptogram() {
        return cryptogram;
    }

    @Override
    public boolean isValidByPrepayment() {
        CreditCard.validate(creditCardNumbers, expirationDate, cryptogram);
        System.out.println("--------------------------------------------");
        System.out.println("Prepayment validation with a 0,00 euros transaction");
        System.out.println("--------------------------------------------\n");
        return true;
    }

    @Override
    public boolean pay(Amount amount, User receiver) {
        System.out.printf("Paying %.2f %s to %s%n", amount.getValue(), amount.getCurrency().getName(), receiver.getFullName());
        return true;
    }
}
