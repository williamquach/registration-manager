package fr.william.quach.domain.repositories;

import fr.william.quach.domain.models.project.Project;
import fr.william.quach.domain.models.project.ProjectId;
import fr.william.quach.infrastructure.errors.project.ProjectAlreadyExistException;
import fr.william.quach.infrastructure.errors.project.ProjectUpdateException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository {

    List<Project> findAll();

    Optional<Project> findById(ProjectId id);

    void createProject(Project project);

    boolean doesProjectAlreadyExist(Project project) throws ProjectAlreadyExistException;

    Project updateProjectStatus(Project project);

    void register(Project project) throws ProjectUpdateException;

    default ProjectId nextId() {
        return ProjectId.fromUUID(UUID.randomUUID());
    }
}
