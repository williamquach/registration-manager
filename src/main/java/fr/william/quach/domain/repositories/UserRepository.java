package fr.william.quach.domain.repositories;

import fr.william.quach.domain.models.user.User;
import fr.william.quach.domain.models.user.UserId;
import fr.william.quach.infrastructure.errors.users.UserAlreadyExistException;
import fr.william.quach.infrastructure.errors.users.UserMembershipGenericException;
import fr.william.quach.infrastructure.errors.users.UserRegistrationException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository {

    List<User> findAll();

    List<User> findAvailableTradesmen();

    Optional<User> findById(UserId id);

    User applyForMembership(User user) throws UserMembershipGenericException;

    boolean doesUserAlreadyExist(User user) throws UserAlreadyExistException;

    User register(User user) throws UserRegistrationException;

    default UserId nextId() {
        return UserId.fromUUID(UUID.randomUUID());
    }
}
