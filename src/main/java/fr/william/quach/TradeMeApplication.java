package fr.william.quach;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class TradeMeApplication implements CommandLineRunner {

	@Autowired
	public ApplicationContextProvider applicationContextProvider;

	public static void main(String[] args) {
		SpringApplication.run(TradeMeApplication.class, args);
	}

	@Override
	public void run(String...args) {
// 		// Create User
//		appContext = applicationContextProvider.getApplicationContext();
//
//		ApplyForMemberShipCommandHandler applyForMemberShipCommandHandler = applicationContextProvider.getApplicationContext().getBean(ApplyForMemberShipCommandHandler.class);
//		ApplyForMembership applyForMembership = new ApplyForMembership(
//				"QUACH", "William",
//				new EmailAddressDTO("william.quach@outlook.fr"), new PasswordDTO("password123"),
//				new CreditCardDTO("1234123412341234", 9, 2022, "233")
//		);
//		final UserId userId = applyForMemberShipCommandHandler.handle(applyForMembership);
//
//		// Retrieve all users
//		RetrieveUsers retrieveUsers = new RetrieveUsers();
//		RetrieveUsersHandler retrieveUsersHandler = applicationContext.getBean(RetrieveUsersHandler.class);
//		final List<UserDTO> users = retrieveUsersHandler.handle(retrieveUsers);
//		users.forEach(System.out::println);
	}
}
